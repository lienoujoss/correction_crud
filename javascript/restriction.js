var nom=document.getElementsByTagName('input')[0];
var prenom=document.getElementsByTagName('input')[1];
var email=document.getElementsByTagName('input')[2];
var nom_utilisateur= document.getElementsByTagName('input')[3];
var telephone= document.getElementsByTagName('input')[4];


var p_alert=document.getElementsByTagName('p')[0];  
var p_nom=document.getElementsByTagName('p')[1];
var p_prenom=document.getElementsByTagName('p')[2];
var p_email=document.getElementsByTagName('p')[3];
var p_nom_utilisateur= document.getElementsByTagName('input')[4];
var p_telephone= document.getElementsByTagName('input')[5];
                
var submit=document.getElementById('submit');
var reset=document.getElementById('reset');
var formulaire=document.getElementById('formulaire');


        function contient_specialchart(char){
                var special = ['@','!','~','<','>','?',' " '," ' ",'*','(',')','^','%','$','#','&','{','}','[',']',';',];

                for (var i = special.length - 1; i >= 0; i--) { //pour chaque element de special
                        for (var id in char) {  // on se rassure qu'il ne soit pas dans char
                                if(char[id]==special[i]){
                                        return true;
                                }
                        }
                }
                return false;
        }


        nom.addEventListener('change', testnom);
        function testnom(){
                // console.log(nom.value);  //verification du fonctionnement
                if(prenom.value.length<3){  //test de taille de contenu
                        alertnom.innerHTML = "Le nom doit avoir au minimum 3 lettres sans les caracteres speciaux tels que @ ou % ...";
                        alertnom.style.opacity='1';   //penser a le faire avec une duree definie. 
                        return false;
                }
                else if(contient_specialchart(prenom.value)){  // test de caracteres non indiques
                        alertnom.innerHTML = "Le nom doit avoir au minimum 3 lettres sans les caracteres speciaux tels que @ ou % ...";
                        alertnom.style.opacity = '1';
                        return false;
                }else{
                        return true;
                }
        }

        prenom.addEventListener('change', testprenom);
        // console.log(prenom);
        function testprenom(){
                // console.log(prenom.value);  //verification du fonctionnement
                if(prenom.value.length<3){  //test de taille de contenu
                        p_prenom.style.display='block'; 
                        return false;
                }
                else if(contient_specialchart(prenom.value)){  // test de caracteres non indiques
                        p_prenom.style.display='block';
                        return false;
                }else{
                        p_prenom.style.display='none';
                        nom.style.backgroundColor='rgb(232,240,254)';
                        return true;;
                }

        nom_utilisateur.addEventListener('change', testnom_utilisateur)
        function testnom_utilisateur(){
                // console.log(nom_utilisateur.value);  //verification du fonctionnement
                if(nom_utilisateur.value.length<3){  //test de taille de contenu
                        p_nom_utilisateur.style.display='block'; 
                        return false;
                }
                else if(contient_specialchart(nom_utilisateur.value)){  // test de caracteres non indiques
                        p_nom_utilisateur.style.display='block';
                        return false;
                }else{
                        p_nom_utilisateur.style.display='none';
                        nom.style.backgroundColor='rgb(232,240,254)';
                        return true;
                }

        telephone.addEventListener('change', testtelephone);
        function testtelephone(){
                if (telephone.value%1 != 0) {  // on se rassure que le champ ne contient que des chiffres.
                        p_telephone.style.display='block';
                        return false;
                }else{
                        p_telephone.style.display='none';
                        nom.style.backgroundColor='rgb(232,240,254)';
                        return true;
                }
                
        }

        email.addEventListener('change', testemail);
        // console.log(email);
        function testemail(){
                        var email_indic='0';
                        var email_format=[],email_ending=[];
                        email_format=['gmail.com','yahoo.com','yahoo.fr','hotmail.com'];
                        var email_ok;
                        // console.log('test');
                        for(var i=0; i<email.value.length; i++){
                                if(email.value[i]=='@'){
                                        email_indic=email.value[i];
                                }
                        }
                        // console.log(email_indic);
                        if(email_indic=='@'){
                                email_ending=email.value.split('@');
                                // console.log(email_ending);
                                for (var i = 0; i < email_format.length; i++) {
                                        // console.log(email_format[i]+' ?= '+email_ending[1]);
                                        if(email_format[i]==email_ending[1]){
                                                email_ok=true;
                                                break;
                                        }
                                        else{
                                                email_ok=false;
                                        }
                                }
                        }
                        else{
                                email_ok=false;
                        }

                        if(email_ok==true){
                                p_email.style.display='none';
                                email.style.backgroundColor='rgb(232,240,254)';
                                return true;
                        }
                        else{
                                p_email.style.display='block';
                                return false;
                        }
                }

        submit.addEventListener('click', function(){
                var check=[];

                        if(testnom()){
                                check[0]=true;
                        }
                        else{
                                check[0]=false;
                        }

                        if(testprenom()){
                                check[1]=true;
                        }
                        else{
                                check[1]=false;
                        }

                        if(testemail()){
                                check[2]=true;
                        }
                        else{
                                check[2]=false;
                        }

                        if(testnom_utilisateur()){
                                check[3]=true;
                        }
                        else{
                                check[3]=false;
                        }

                        if(testtelephone()){
                                check[4]=true;
                        }
                        else{
                                check[4]=false;
                        }

                        if(check[0]&&check[1]&&check[2]&&check[3]&&check[4]){
                                formulaire.submit();
                        }
                        else{
                                p_alert.style.display='block';
                        }
        });     

        reset.addEventListener('click', function(){
                p_alert.style.display='none';
                p_nom.style.display='none';
                p_prenom.style.display='none';
                p_mail.style.display='none';
                p_nom_utlisateur.style.display='none';
                p_telephone.style.display='none';
                formulaire.reset();

        });