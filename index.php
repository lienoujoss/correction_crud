<?php 
	session_start();
	if (isset($_SESSION['USER'])) {
		header('location:pages/profil.php');
	} else {


?>

<!DOCTYPE html>
<html>
<head>
	<title>Formulaire</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/formulaire.css">
	<link rel="stylesheet" type="text/css" href="css/font-awesome.css">
	<meta name="viewport" content="width-device-width, initial-scale=1. shrink-to-fit=no">
</head>
<body>
	<div class="container-fluid">
		<div class="row entete">
			<div class="nav navbar-nav col-md-6 col-sm-8 col-xs-4">
				<div class="col-md-3 nav navbar-nav">
					<img src="photo/inc3.jpg" class="img-circle pull pull-right logo">
				</div>

				<div class="col-md-9 slogan">
					<p>Giving Life Support To Customers <br>
						Votre Service Client...</p>
				</div>
			</div>


			<div class="nav navbar-nav col-md-6 col-sm-9 col-xs-9 pull pull-right">
				<button class="btn btn-info pull pull-right B1"><span><span><a href="pages/connexion.php"><span>Connexion</span></a></button>
				
				<button class="btn btn-info pull pull-right B1"><span><span><a href="pages/inscription.php"><span>Inscription</span></a></button>
				
				<form class="navbar-form pull pull-right B2">
					<input type="text" class="form-control" placeholder="Rechercher sur Tech-Inc...">
					<button class="btn btn-info B3"><span class="glyphicon glyphicon-search"></span><span>Search</span></button>
				</form>
			</div>
		</div>

				<!-- *************************************FIN DE L'ENTETE****************************** -->
				<!-- *************************************DEBUT DU BODY****************************** -->

		<div class="row ix1">
			<div class="col-md-offset-2 col-md-9">
				<div class="panel ix11">
					<div class="panel-body">
						<video class="video v" autoplay loop muted>
          					<source src="photo/h.mp4" type="video/mp4"/>
        				</video>	
					</div>
				</div>
			</div>
		</div>

		<div class="row" style="margin-bottom:100px;">
			<div class="col-md-6">
				<div class="panel bloc1">
					<div class="panel-heading">
						<h1 class="pan-h1">Qui Sommes-nous</h1>
					</div>
					<div class="panel-body">
						<p class="pan-p1"><span class="marque">Tech-Inc</span> est un service client de qualité fiable qui repond aux besoins de sa clientèle en temps réel au moyen de diverses prestations Techniques & Commerciales, dont le but est la reduction de l'effort client et sa Satisfaction</p>

						<button type="button" class="btn btn-danger B4">
  							Savoir Plus
						</button>	
					</div>
				</div>
			</div>

			<div class="col-md-6 bloc2">
				<img src="photo/tech1.png">
			</div>
		</div>

		<div class="row footer">
			<div class="col-md-12">
				<div class="col-md-3 bloc-footer">
					<div class="row">
						<div class="col-md-12 blocf">
							Pourquoi nous?
						</div>
					</div>
					<div class="row">
						<div class="col-md-12 bloc1-footer">
							<ul style="list-style:none">
								<li><span class="glyphicon glyphicon-chevron-right icon"></span>&nbsp <span class="p-footer">Satisfaction</span></li>
								<li><span class="glyphicon glyphicon-chevron-right icon"></span>&nbsp <span class="p-footer">Sécurité</span></li>
								<li><span class="glyphicon glyphicon-chevron-right icon"></span>&nbsp <span class="p-footer">Fiabilité</span></li>
								<li><span class="glyphicon glyphicon-chevron-right icon"></span>&nbsp <span class="p-footer">Garantie</span></li>
							</ul>
						</div>
					</div>
				</div>

				<div class="col-md-3 bloc-footer">
					<div class="row">
						<div class="col-md-12 blocf">
							Adresses
						</div>
					</div>
					<div class="row">
						<div class="col-md-12 bloc2-footer">
							<ul class=" " style="list-style:none">
								<li><span class="glyphicon glyphicon-globe icon"></span>&nbsp Pays: &nbsp <span class="p-footer">Cameroun</span></li>

								<li><span class="glyphicon glyphicon-map-marker icon"></span>&nbsp Ville: &nbsp <span class="p-footer">Douala</span></li>

								<li><span class="glyphicon glyphicon-phone icon"></span>&nbsp Téléphone: &nbsp <span class="p-footer">(+237) 671 316 424</span></li>

								<li><span class="glyphicon glyphicon-envelope icon"></span>&nbsp Email:&nbsp <span class="p-footer">tech.inc.cm@gmail.com</span></li>
							</ul>
						</div>
					</div>
				</div>

				<div class="col-md-3 bloc-footer">
					<div class="row">
						<div class="col-md-12 blocf">
							Suivez-Nous
						</div>
					</div>
					<div class="row">
						<div class="col-md-12 bloc3-footer">
							<ul style="list-style:none">
								<li><span><i class="fa fa-facebook facebook"></i>&nbsp Facebook: <a href=""><span class="p-footer">Inch Class</span></a></li>

								<li><span><i class="fa fa-twitter twitter"></i></span>&nbsp Twitter: &nbsp <a href=""><span class="p-footer">Inch Class</span></a></li>

								<li><span><i class="fa fa-linkedin linkedin"></i></span>&nbsp LinkedIn: &nbsp <a href=""><span class="p-footer">Inch Class</span></a></li>

								<li><span><i class="fa fa-whatsapp whatsapp"></i></span></span>&nbsp Email:&nbsp <a href=""><span class="p-footer">(+237) 671 316 424</span></a></li>
							</ul>
						</div>
					</div>
				</div>

				<div class="col-md-3 bloc-footer">
					<div class="row Z1">
						<div class="col-md-12">
							<label class="bloc4">Newsletter</label>
							<div class="input-group bloc5">
								<form>	
									<input class="form-control bloc5" type="text" name="text" placeholder="Type Your Email Yere" style="font-style:italic; font-family:arial narrow;">
									<button class="btn pull pull-right bf">Subscribe</button>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="col-md-12">
				<ul class="row2">
					<li>Copyright © 2021 Tech-Inc</li>
					<li>Powered by Tech-Inc</li>
				</ul>
			</div>
		</div>
	</div>
	<script type="text/javascript" src="javascript/jquery-3.6.0.min.js"></script>
	<script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="javascript/javascript.js"></script>
</body>
</html>
<?php } ?>