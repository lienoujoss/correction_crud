<?php 
	session_start();
	if (isset($_SESSION['USER'])) {
		header('location:profil.php');
	} else {


?>

<!DOCTYPE html>
<html>
<head>
	<title>Formulaire</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="../css/formulaire.css">
	<link rel="stylesheet" type="text/css" href="../css/font-awesome.css">
	<meta name="viewport" content="width-device-width, initial-scale=1. shrink-to-fit=no">
</head>
<body>
	<div class="container-fluid">
		<div class="row entete">
			<div class="nav navbar-nav col-md-6 col-sm-8 col-xs-4">
				<div class="col-md-3 nav navbar-nav">
					<img src="../photo/inc2.png" class="img-circle pull pull-right logo">
				</div>

				<div class="col-md-9 slogan">
					<p>Giving Life Support To Customers <br>
						Votre Service Client...</p>
				</div>
			</div>


			<div class="nav navbar-nav col-md-6 col-sm-9 col-xs-9 pull pull-right menu">
				<button class="btn btn-info pull pull-right B1"><span><span><a href="inscription.php"><span>Inscription</span></a></button>
				
				<button class="btn btn-info pull pull-right B1"><span><span><a href="../index.php"><span>Acceuil</span></a></button>
				
				<form class="navbar-form pull pull-right B2">
					<input type="text" class="form-control" placeholder="Rechercher sur Tech-Inc...">
					<button class="btn btn-info B3"><span class="glyphicon glyphicon-search"></span><span>Search</span></button>
				</form>
			</div>
		</div>

				<!-- *************************************FIN DE L'ENTETE****************************** -->
				<!-- *************************************DEBUT DU BODY****************************** -->

		<div class="row">
			<div class=" form1 col-md-offset-2 col-md-8  col-xs-12 col-xm-offset-1 col-xm-11" style="margin-top:100px; margin-bottom: 100px;">
				<h1>CONNECTEZ VOUS</h1>
				<p style="text-align: center; color:#850404;">
					<?php 
						if (!empty($_SESSION['errormail']) AND !empty($_SESSION['errorpass'])){
							echo (" Cet Utilisateur N'existe pas !");
						}
					?>
				</p>
				<form enctype="multipart/form-data" method="post" action="traitement_connexion.php" id="myform"  class="formulaire">
					<div class="col-md-5 col-xm-5 col-xs-offset-1 col-xs-11 form2">
						<span class="glyphicon glyphicon-envelope"></span>
						<label>Email</label>
						<input class="form-control inpt3" type="email" name="email" placeholder="veuillez entrer votre adresse mail" id="mail" required="">
							<p style="text-align: center; color:#850404;">
								<?php

								    if ( isset($_SESSION['errormail']) AND empty($_SESSION['errorpass']) ){
									    echo ($_SESSION['errormail']);
									} 

								?>
							</p>
					</div>
								
					<div class="col-md-offset-1 col-md-5 col-md-pull-1 col-xm-5 col-xs-offset-1 col-xs-11 form3">
										
						<span class="glyphicon glyphicon-lock"></span>
							<label>Mot de passe</label>
							<input class="form-control inpt4" type="password" name="pwd" placeholder="Veuillez entrer votre mot de passe" id="pwd"  required="">
								<p style="text-align: center; color:#850404;">
									<?php

									    if ( isset($_SESSION['errormail']) AND empty($_SESSION['errorpass']) ){
										    echo ($_SESSION['errorpass']);
										} 

									?>
								</p> 
					</div>

					<div class="row" style="text-align:center; margin:20px;">
						<input  type="submit" name="Connexion"  class="btn btn-info btn-success bout1" style="margin:20px;">
					</div>
				</form>		
			</div>	
		</div>

				<!-- *************************************FIN DU BODY****************************** -->
				<!-- *************************************DEBUT DU FOOTER****************************** -->

		<div class="row footer">
			<div class="col-md-12">
				<div class="col-md-3 bloc-footer">
					<div class="row">
						<div class="col-md-12 blocf">
							Pourquoi nous?
						</div>
					</div>
					<div class="row">
						<div class="col-md-12 bloc1-footer">
							<ul style="list-style:none">
								<li><span class="glyphicon glyphicon-chevron-right icon"></span>&nbsp <span class="p-footer">Satisfaction</span></li>
								<li><span class="glyphicon glyphicon-chevron-right icon"></span>&nbsp <span class="p-footer">Sécurité</span></li>
								<li><span class="glyphicon glyphicon-chevron-right icon"></span>&nbsp <span class="p-footer">Fiabilité</span></li>
								<li><span class="glyphicon glyphicon-chevron-right icon"></span>&nbsp <span class="p-footer">Garantie</span></li>
							</ul>
						</div>
					</div>
				</div>

				<div class="col-md-3 bloc-footer">
					<div class="row">
						<div class="col-md-12 blocf">
							Adresses
						</div>
					</div>
					<div class="row">
						<div class="col-md-12 bloc2-footer">
							<ul class=" " style="list-style:none">
								<li><span class="glyphicon glyphicon-globe icon"></span>&nbsp Pays: &nbsp <span class="p-footer">Cameroun</span></li>

								<li><span class="glyphicon glyphicon-map-marker icon"></span>&nbsp Ville: &nbsp <span class="p-footer">Douala</span></li>

								<li><span class="glyphicon glyphicon-phone icon"></span>&nbsp Téléphone: &nbsp <span class="p-footer">(+237) 671 316 424</span></li>

								<li><span class="glyphicon glyphicon-envelope icon"></span>&nbsp Email:&nbsp <span class="p-footer">tech.inc.cm@gmail.com</span></li>
							</ul>
						</div>
					</div>
				</div>

				<div class="col-md-3 bloc-footer">
					<div class="row">
						<div class="col-md-12 blocf">
							Suivez-Nous
						</div>
					</div>
					<div class="row">
						<div class="col-md-12 bloc3-footer">
							<ul style="list-style:none">
								<li><span><i class="fa fa-facebook facebook"></i>&nbsp Facebook: <a href=""><span class="p-footer">Inch Class</span></a></li>

								<li><span><i class="fa fa-twitter twitter"></i></span>&nbsp Twitter: &nbsp <a href=""><span class="p-footer">Inch Class</span></a></li>

								<li><span><i class="fa fa-linkedin linkedin"></i></span>&nbsp LinkedIn: &nbsp <a href=""><span class="p-footer">Inch Class</span></a></li>

								<li><span><i class="fa fa-whatsapp whatsapp"></i></span></span>&nbsp Email:&nbsp <a href=""><span class="p-footer">(+237) 671 316 424</span></a></li>
							</ul>
						</div>
					</div>
				</div>

				<div class="col-md-3 bloc-footer">
					<div class="row">
						<div class="col-md-12">
							<label class="bloc4">Newsletter</label>
							<div class="input-group bloc5">
								<form>	
									<input class="form-control" type="text" name="text" placeholder="Type Your Email Yere" style="font-style:italic; font-family:arial narrow">
									<button class="btn pull pull-right bf">Subscribe</button>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="col-md-12">
				<ul class="row2">
					<li>Copyright © 2021 Tech-Inc</li>
					<li>Powered by Tech-Inc</li>
				</ul>
			</div>
		</div>
	</div>
<?php 
	session_destroy();
 ?>
	<script type="text/javascript" src="../javascript/jquery-3.6.0.min.js"></script>
	<script type="text/javascript" src="../bootstrap/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="../javascript/javascript.js"></script>
	<script type="text/javascript">

		pwd.addEventListener('change', testpwd);
		function testpwd(){
			if(pwd.value.length<8){
				// pwd.setCustomValidity('Veuillez entrer un mot de passe comprenant au moins 8 lettres et caracteres speciaux');2
				return false;
			}else{
				return true;
			}

		}

		var mail = document.getElementById('mail');

		mail.addEventListener('blur', testmail);
		function testmail(){
			var mail_indic='0';
			var mail_format=[],mail_ending=[];
			mail_format=['gmail.com','yahoo.com','yahoo.fr','hotmail.com' , 'icloud.com']; 

			for(var i=0; i<mail.value.length; i++){  
				if(mail.value[i]=='@'){	
					mail_indic=mail.value[i];
				}
			}
			console.log(mail_indic);
			if(mail_indic=='@'){         
				mail_ending=mail.value.split('@');  
				
				for (var i = 0; i < mail_format.length; i++) { 
					if(mail_format[i]==mail_ending[1]){	
						return true;
					}else{
						if(i==mail_format.length-1 && mail_format[i]!=mail_ending[1]){  
							// mail.setCustomValidity('Le format de mail attendu est :exemple@xmail.xxx');
							return false;
						}
					}
				}
			}else{ 	
				// mail.setCustomValidity('Le format de mail attendu est exemple@xmail.xxx');
				return false;
			}
		}


	</script>
</body>
</html>

<?php } ?>