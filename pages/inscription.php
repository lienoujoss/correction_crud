<?php 
	session_start();
?>
<!DOCTYPE html>
<html>
<head>
	<title>Formulaire</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="../css/formulaire.css">
	<link rel="stylesheet" type="text/css" href="../css/font-awesome.css">
	<meta name="viewport" content="width-device-width, initial-scale=1. shrink-to-fit=no">
</head>
<body>
	<div class="container-fluid">
		<div class="row entete">
			<div class="nav navbar-nav col-md-6 col-sm-8 col-xs-4">
				<div class="col-md-3 nav navbar-nav">
					<img src="../photo/inc2.png" class="img-circle pull pull-right logo">
				</div>

				<div class="col-md-9 slogan">
					<p>Giving Life Support To Customers <br>
						Votre Service Client...</p>
				</div>
			</div>


			<div class="nav navbar-nav col-md-6 col-sm-9 col-xs-9 pull pull-right menu">
				<button class="btn btn-info pull pull-right B1"><span><span><a href="connexion.php"><span>Connexion</span></a></button>
				
				<button class="btn btn-info pull pull-right B1"><span><span><a href="../index.php"><span>Acceuil</span></a></button>
				
				<form class="navbar-form pull pull-right B2">
					<input type="text" class="form-control" placeholder="Rechercher sur Tech-Inc...">
					<button class="btn btn-info B3"><span class="glyphicon glyphicon-search"></span><span>Search</span></button>
				</form>
			</div>
		</div>

				<!-- *************************************FIN DE L'ENTETE****************************** -->
				<!-- *************************************DEBUT DU BODY****************************** -->

		<div class="row ix1">
			<div class=" form1 col-md-offset-2 col-md-8  col-xs-12 col-xm-offset-2 col-xm-10" style="margin-top:100px; margin-bottom: 100px;">
				<h1>INSCRIVEZ VOUS</h1>
				<p style="text-align:center;">C'est gratuit et ça le restera toujours!!</p>
                <h4 class="text-center" style="color: red;">
                     <?php 
                         if (isset($_SESSION['message_error'])) {
                             echo ($_SESSION['message_error']);
                         }
                      ?>
                </h4>
				<form enctype="multipart/form-data" method="post" action="traitement.php" id="myform" class="formulaire">
					<div class="row">
						<div class="col-md-5 col-xm-5 col-xs-offset-1 col-xs-11 form2">
							<span class="glyphicon glyphicon-user"></span>
							<label>Noms</label>
							<div class="input-group">
								<input id="name" class=" form-control inpt1 " type="text" name="nom" placeholder="veuillez entrer votre nom"  required="">
								<span class="validity input-group-addon" style="background-color: none;"></span>
								<p></p>
							</div>

							<span class="glyphicon glyphicon-user"></span>
							<label>Prenoms</label>
							<div class="input-group">
								<input class="form-control inpt2" type="text" name="prenom" placeholder="veuillez entrer votre nom"  id="prenom" required="">
								<span class="validity input-group-addon" style="background-color: none;"></span>
								<p></p>
							</div>

							<span class="glyphicon glyphicon-envelope"></span>
							<label>Email</label>
							<div class="input-group">
								<input class="form-control inpt3" type="email" name="email" placeholder="veuillez entrer votre adresse mail" id="mail" required="">
								<span class="validity input-group-addon" style="background-color: none;"></span>
								<p></p>
							</div>

							<span class="glyphicon glyphicon-lock"></span>
							<label>Mot de passe</label>
							<div class="input-group">
								<input class="form-control inpt4" type="password" name="pwd" placeholder="Veuillez entrer votre mot de passe" id="pwd"  required="">
								<span class="validity input-group-addon" style="background-color: none;"></span>
							</div>
							<input type="hidden" name="niveau" value="1"><br>
						</div>

						<div class="col-md-offset-1 col-md-5 col-md-pull-1 col-xm-5 col-xs-offset-1 col-xs-11 form3">

							<span class="glyphicon glyphicon-picture"></span>
							<label>Telecharger votre photo de pofil</label>
							<div class="input-group">
								<input class="form-control inpt7" type="file" name="photo" accept="image/*" onchange="loadFile(event)" required="" >
								<span class="validity input-group-addon" style="background-color: none;"></span>
							</div>
							<div class="col-md-offset-4 col-md-4 im" style="">
	                        	<img id="im"/>
	                  		</div>
						</div>
					</div>
					<div class="row" style="text-align:center; margin:20px;">
						<input  type="reset" name="Annuler" class="btn btn-info bout1" style="margin:20px;">
						<input  type="submit" name="Enregistrer"  class="btn btn-info btn-success bout1" style="margin:20px;"> 
					</div>
				</form>		
			</div>
		</div>

		<div class="row footer">
			<div class="col-md-12">
				<div class="col-md-3 bloc-footer">
					<div class="row">
						<div class="col-md-12 blocf">
							Pourquoi nous?
						</div>
					</div>
					<div class="row">
						<div class="col-md-12 bloc1-footer">
							<ul style="list-style:none">
								<li><span class="glyphicon glyphicon-chevron-right icon"></span>&nbsp <span class="p-footer">Satisfaction</span></li>
								<li><span class="glyphicon glyphicon-chevron-right icon"></span>&nbsp <span class="p-footer">Sécurité</span></li>
								<li><span class="glyphicon glyphicon-chevron-right icon"></span>&nbsp <span class="p-footer">Fiabilité</span></li>
								<li><span class="glyphicon glyphicon-chevron-right icon"></span>&nbsp <span class="p-footer">Garantie</span></li>
							</ul>
						</div>
					</div>
				</div>

				<div class="col-md-3 bloc-footer">
					<div class="row">
						<div class="col-md-12 blocf">
							Adresses
						</div>
					</div>
					<div class="row">
						<div class="col-md-12 bloc2-footer">
							<ul class=" " style="list-style:none">
								<li><span class="glyphicon glyphicon-globe icon"></span>&nbsp Pays: &nbsp <span class="p-footer">Cameroun</span></li>

								<li><span class="glyphicon glyphicon-map-marker icon"></span>&nbsp Ville: &nbsp <span class="p-footer">Douala</span></li>

								<li><span class="glyphicon glyphicon-phone icon"></span>&nbsp Téléphone: &nbsp <span class="p-footer">(+237) 671 316 424</span></li>

								<li><span class="glyphicon glyphicon-envelope icon"></span>&nbsp Email:&nbsp <span class="p-footer">tech.inc.cm@gmail.com</span></li>
							</ul>
						</div>
					</div>
				</div>

				<div class="col-md-3 bloc-footer">
					<div class="row">
						<div class="col-md-12 blocf">
							Suivez-Nous
						</div>
					</div>
					<div class="row">
						<div class="col-md-12 bloc3-footer">
							<ul style="list-style:none">
								<li><span><i class="fa fa-facebook facebook"></i>&nbsp Facebook: <a href=""><span class="p-footer">Inch Class</span></a></li>

								<li><span><i class="fa fa-twitter twitter"></i></span>&nbsp Twitter: &nbsp <a href=""><span class="p-footer">Inch Class</span></a></li>

								<li><span><i class="fa fa-linkedin linkedin"></i></span>&nbsp LinkedIn: &nbsp <a href=""><span class="p-footer">Inch Class</span></a></li>

								<li><span><i class="fa fa-whatsapp whatsapp"></i></span></span>&nbsp Email:&nbsp <a href=""><span class="p-footer">(+237) 671 316 424</span></a></li>
							</ul>
						</div>
					</div>
				</div>

				<div class="col-md-3 bloc-footer">
					<div class="row">
						<div class="col-md-12">
							<label class="bloc4">Newsletter</label>
							<div class="input-group bloc5">
								<form>	
									<input class="form-control" type="text" name="text" placeholder="Type Your Email Yere" style="font-style:italic; font-family:arial narrow">
									<button class="btn pull pull-right bf">Subscribe</button>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="col-md-12">
				<ul class="row2">
					<li>Copyright © 2021 Tech-Inc</li>
					<li>Powered by Tech-Inc</li>
				</ul>
			</div>
		</div>
	</div>
	<script type="text/javascript" src="../javascript/jquery-3.6.0.min.js"></script>
	<script type="text/javascript" src="../bootstrap/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="../javascript/javascript.js"></script>

	<script>
  	var loadFile = function(event) {
        var profil = document.getElementById('im');
        profil.src = URL.createObjectURL(event.target.files[0]);
      };
  	</script>
</body>
</html>