<?php
	 session_start();
	 $nom = $_SESSION['USER']['nom'];
	$prenom = $_SESSION['USER']['prenom'];
	$email = $_SESSION['USER']['email'];
	$pswd = $_SESSION['USER']['pwd'];
	$image = $_SESSION['USER']['photo'];
?>

<!DOCTYPE html>
<html>
<head>
	<title>Profil</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="../css/formulaire.css">
	<link rel="stylesheet" type="text/css" href="../css/ionicons.min.css">
	<link rel="stylesheet" type="text/css" href="../css/font-awesome.css">
	<meta name="viewport" content="width-device-width, initial-scale=1. shrink-to-fit=no">

	<style>

		@media screen and (max-width: 768px){
			.B2{
				display: none;
			}

			.slogan{
				display:none;
			}

		}

		@media only screen and (max-width: 760px){
			.logo{
				margin-right:260px;
			}

			.profil{
				margin-top:-120px;
			}

			.B2{
				display:none;
			}

			.H{
				color:white;
			}

			.slogan{
				display:none;
			}

			.B1{
				margin-right:10px;
				padding:15px;
			}

			.blocf{
				font-size:15px;
			}

			.bloc1-footer{
				font-size:10px;
			}

			.bloc2-footer{
				font-size:10px;
			}

			.bloc3-footer{
				font-size:10px;
			}

			.bloc-footer{
				font-size:15px;
			}

			.bloc5{
				font-size:10px;
				width:50%;
			}

			.bf{
				font-size:10px;
				width:70%;
			}

			.row2{
				font-size:10px;
			}

			.Z1{
				margin-left:95px;
			}

			
		}

		/* Button used to open the chat form - fixed at the bottom of the page */
		.open-button {;
		  border: none;
		  cursor: pointer;
		}

		/* The popup chat - hidden by default */
		.chat-popup {
		  display: none;
		  border: 3px solid #f1f1f1;
		  z-index: 9;
		}

		/* Add styles to the form container */
		.form-container {
		  background-color: white;
		}

		/* Full-width textarea */
		.form-container input {
		  /*width: 100%;
		  padding: 15px;
		  margin: 5px 0 22px 0;*/
		  border: none;
		  background: #f1f1f1;
		  resize: none;
		}

		/* When the textarea gets focus, do something */
		.form-container input:focus {
		  background-color: #ddd;
		  outline: none;
		}

		/* Set a style for the submit/send button */
		.form-container .boutw {
		  border: none;
		  cursor: pointer;
		}

		/* Add a red background color to the cancel button */
		.form-container .cancel {
		  background-color: red;
		}

		/* Add some hover effects to buttons */
		.form-container .boutw:hover, .open-button:hover {
		  opacity: 1;
		}
	</style>
</head>
<body>
	<div class="container-fluid">
		<div class="row entete">
			<div class="nav navbar-nav col-md-4">
				<div class="col-md-3 nav navbar-nav">
					<img src="../photo/inc2.png" class="img-circle pull pull-right logo">
				</div>

				<div class="col-md-9 slogan">
					<p>Giving Life Support To Customers <br>
						Votre Service Client...</p>
				</div>
			</div>


			<div class="nav navbar-nav col-md-8 pull pull-right menu">
				<ul class="nav navbar-nav pull pull-right">
					<li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle profil"  id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						    <?php echo "<img class='img-circle pull pull-right' style='width:60px; height:60;; margin-right:20px' src='../images/".$_SESSION['USER']['photo']."'>" ?>
						    <p><span class="nom"> <?php echo $_SESSION['USER']['nom']." ".$_SESSION['USER']['prenom'] ?></span></p>
						</a>

						<ul class="dropdown-menu pull pull-right">
							   <li class="divider"></li>
							   <li><a href="profil.php">Mon profil</a></li>
							   <li class="dropdown-divider"></li>
							   <li><a href="deconnexion.php">Deconnexion</a></li>
							  <li class="divider"></li>
						</ul>
					</li>
				</ul>
			</div>
				
		</div>

							<!-- *************************************FIN DE L'ENTETE****************************** -->
							<!-- *************************************DEBUT DU BODY****************************** -->

		<div class="row">
			

				<div class="col-md-offset-5 col-md-2 col-sm-offset-2 col-sm-8 col-xs-offset-2 col-xs-8" style="text-align:center;" >

					<div>
						<h3 class="H">Mes Informations Personnelles</h3>
						<nav style="margin-top:20px;">
	    					<?php echo "<img style='width:60px; height:60;' class='profil' src='../images/".$_SESSION['USER']['photo']."'>" ?>
	    				</nav>

	    				<nav style="margin-top:20px;">
					    <label for="nom">Nom :<?php echo $_SESSION['USER']['nom'] ?></label><br>
					    <label for="prenom">Prenom :<?php echo $_SESSION['USER']['prenom'] ?></label><br>
					    <label for="email">Email :<?php echo $_SESSION['USER']['email'] ?></label><br>
					    <label for="pass">Mot de passe actuel :<?php echo $_SESSION['USER']['pwd'] ?></label><br>
					    <button class="pull pull-right open-button btn btn-success glyphicon glyphicon-pencil" onclick="openForm()">Modifier</button>
					    </nav>
				    </div>


				    <div class="chat-popup" id="myForm">

	                    <h3>Votre Profil</h3>
		                    <form class="form-container" action="traitement_modification_profil.php" method="post" enctype="multipart/form-data" >
		                        <label for="nom">Nom :</label>
		                        <input id="nom" class="form-control" type="text" name="nom" value="<?php echo $_SESSION['USER']['nom'] ?>" required>
		                        <label for="prenom">Prenom :</label>
		                        <input id="prenom" class="form-control" type="text" name="prenom" value="<?php echo $_SESSION['USER']['prenom'] ?>" required>
		                        <label for="email">Email :</label>
		                        <input id="email" class="form-control" type="text" name="email" value="<?php echo $_SESSION['USER']['email'] ?>" required>
		                        <label for="pass">Mot de passe actuel :</label>
		                        <input id="pass" class="form-control" type="password" name="pwd1" value="<?php echo $_SESSION['USER']['pwd'] ?>">
		                        <label for="pass1">Nouveau Mot de passe :</label>
		                        <input id="pass1" class="form-control" type="password" name="pwd2" placeholder ="Entrez votre nouveau mot de passe" required>
		                        <label>Telecharger votre nouvelle photo de pofil</label>
		                        <input class="form-control inpt7" type="file" name="photo" accept="image/*" onchange="loadFile(event)" required="" >
		                        <div class="col-md-offset-4 col-md-4 im" style="">
		                                    <img id="im"/>
		                                    </div>
		                        <div class="col-md-offset-2 col-md-4 col-xm-5 col-xs-6 ">
		                            <button type="button" class="boutw btn btn-danger" onclick="closeForm()">Fermer</button>
		                        </div>
		                        <div class="  col-md-4 col-xm-5 col-xs-6">
		                            <button type="submit" class="boutw btn btn-success">Enregistrer</button>
		                        </div>
		                    </form>
	                </div>
	           	</div>


			
		</div>

						<!-- *************************************FIN DU BODY****************************** -->
						<!-- *************************************DEBUT DU FOOTER****************************** -->

		<div class="row footer">
			<div class="col-md-12">
				<div class="col-md-3 bloc-footer">
					<div class="row">
						<div class="col-md-12 blocf">
							Pourquoi nous?
						</div>
					</div>
					<div class="row">
						<div class="col-md-12 bloc1-footer">
							<ul style="list-style:none">
								<li><span class="glyphicon glyphicon-chevron-right icon"></span>&nbsp <span class="p-footer">Satisfaction</span></li>
								<li><span class="glyphicon glyphicon-chevron-right icon"></span>&nbsp <span class="p-footer">Sécurité</span></li>
								<li><span class="glyphicon glyphicon-chevron-right icon"></span>&nbsp <span class="p-footer">Fiabilité</span></li>
								<li><span class="glyphicon glyphicon-chevron-right icon"></span>&nbsp <span class="p-footer">Garantie</span></li>
							</ul>
						</div>
					</div>
				</div>

				<div class="col-md-3 bloc-footer">
					<div class="row">
						<div class="col-md-12 blocf">
							Adresses
						</div>
					</div>
					<div class="row">
						<div class="col-md-12 bloc2-footer">
							<ul class=" " style="list-style:none">
								<li><span class="glyphicon glyphicon-globe icon"></span>&nbsp Pays: &nbsp <span class="p-footer">Cameroun</span></li>

								<li><span class="glyphicon glyphicon-map-marker icon"></span>&nbsp Ville: &nbsp <span class="p-footer">Douala</span></li>

								<li><span class="glyphicon glyphicon-phone icon"></span>&nbsp Téléphone: &nbsp <span class="p-footer">(+237) 671 316 424</span></li>

								<li><span class="glyphicon glyphicon-envelope icon"></span>&nbsp Email:&nbsp <span class="p-footer">tech.inc.cm@gmail.com</span></li>
							</ul>
						</div>
					</div>
				</div>

				<div class="col-md-3 bloc-footer">
					<div class="row">
						<div class="col-md-12 blocf">
							Suivez-Nous
						</div>
					</div>
					<div class="row">
						<div class="col-md-12 bloc3-footer">
							<ul style="list-style:none">
								<li><span><i class="fa fa-facebook facebook"></i>&nbsp Facebook: <a href=""><span class="p-footer">Tech-Inc</span></a></li>

								<li><span><i class="fa fa-twitter twitter"></i></span>&nbsp Twitter: &nbsp <a href=""><span class="p-footer">TechInc7</span></a></li>

								<li><span><i class="fa fa-linkedin linkedin"></i></span>&nbsp LinkedIn: &nbsp <a href=""><span class="p-footer">Tech-Inc</span></a></li>

								<li><span><i class="fa fa-whatsapp whatsapp"></i></span></span>&nbsp Whatsapp:&nbsp <a href=""><span class="p-footer">(+237) 671 316 424</span></a></li>
							</ul>
						</div>
					</div>
				</div>

				<div class="col-md-3 bloc-footer">
					<div class="row">
						<div class="col-md-12">
							<label class="bloc4">Newsletter</label>
							<div class="input-group bloc5">
								<form>	
									<input class="form-control" type="text" name="text" placeholder="Type Your Email Yere" style="font-style:italic; font-family:arial narrow">
									<button class="btn pull pull-right bf">Subscribe</button>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="col-md-12">
				<ul class="row2" style="list-style:none; text-align:center;">
					<li>Copyright © 2021 Tech-Inc</li>
					<li>Powered by Tech-Inc</li>
				</ul>
			</div>
		</div>
		
</div>
	<script type="text/javascript" src="../javascript/jquery.min.js"></script>
	<script type="text/javascript" src="../javascript/bootstrap.min.js"></script>
	<script type="text/javascript">
		var formulaire = document.getElementById('form');
		var modif = document.getElementById('modif');
		var input = document.getElementsByTagName('input');
		var Send = document.getElementById('Send'); 
		var profil = document.getElementById('pp');
		var BlocPhoto = document.getElementById('ppp');
		var source = input[0].getAttribute('src');
		console.log(BlocPhoto);
		console.log(profil);
		

    // previsualisation image dans formulaire
			var loadFile = function(event) {
      profil.src = URL.createObjectURL(event.target.files[0]);
    };


		// Detection Appui Boutton Send
		Send.addEventListener('click' , function(){
			input[3].removeAttribute('disabled');
			formulaire.submit();
		});


		// Evenement sur nom
		input[1].addEventListener('blur' , function(){
			
		})

		// Evenement sur prenom
		input[2].addEventListener('blur' , function(){
			
		})


		// Evenement sur password
		input[4].addEventListener('blur' , function(){
			
		});

		//Evenement sur annuler
		input[5].addEventListener('click', function(){
			BlocPhoto.removeChild(BlocPhoto.lastElementChild);
			BlocPhoto.appendChild(profil);
			console.log('ici BlocPhoto'+BlocPhoto);
		});

		// Evenement sur image
		input[0].addEventListener('change' , function(){
			
		});

		// desactivation modification profil
		function Masque() {
			for (var i = input.length - 3; i >= 0; i--) {
					input[i].setAttribute('disabled','true');
				}
			input[5].style.display= 'none';	
			input[6].style.display= 'none';	
		}
		Masque();

		// activation modification profil
		modif.addEventListener('click' , function(){
			for (var i = input.length - 3; i >= 0; i--) {
					if (i==3) {
						continue;
					} else{
						input[i].removeAttribute('disabled');
					}
				}
			input[5].style.display= 'block';	
			input[6].style.display= 'block';	
		});
	</script>
	<?php 
		unset($_SESSION['message_error']);
	?>

	<script>
		function openForm() {
		  document.getElementById("myForm").style.display = "block";
		}

		function closeForm() {
		  document.getElementById("myForm").style.display = "none";
		}
	</script>


    <script type="text/javascript">
      	var loadFile = function(event) {
        var profil = document.getElementById('im');
          	profil.src = URL.createObjectURL(event.target.files[0]);
        };
    </script>
</body>
</html>